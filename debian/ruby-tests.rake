require 'gem2deb/rake/spectask'
require 'open-uri'
require 'tmpdir'

ENV['HOME'] = Dir.mktmpdir
ENV['LC_ALL'] = 'C.UTF-8'

specs_need_internet = [
  './spec/jekyll-remote-theme/integration_spec.rb',
  './spec/jekyll-remote-theme/downloader_spec.rb',
  './spec/jekyll-remote-theme/munger_spec.rb',
  './spec/jekyll_remote_theme_spec.rb'
]

Gem2Deb::Rake::RSpecTask.new(:spec) do |spec|
  spec.pattern  = FileList['./spec/**/*_spec.rb'] - specs_need_internet
  begin
    spec.pattern += specs_need_internet if URI.open("https://github.com/pages-themes/primer")
  rescue SocketError
    # Do nothing
  end
end

task :default => :spec
